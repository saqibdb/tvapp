//
//  SettingsViewController.m
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsViewCell.h"
#import "CropViewController.h"
#import "Model.h"

@interface SettingsViewController () <UITableViewDataSource, UITableViewDelegate, SettingsViewCellDelegate, CropViewDelegate> {
    __weak IBOutlet UIView *gradientView;
    CAGradientLayer* gradient;
    Model* model;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    gradient = [[CAGradientLayer alloc] init];
//    gradient.frame = gradientView.bounds;
//    gradient.colors = @[(id)[UIColor colorWithRed:237/255.0 green:46/255.0 blue:99/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:205/255.0 green:0 blue:63/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:181/255.0 green:0 blue:35/255.0 alpha:1].CGColor];
//    gradient.locations = @[@0, @0.5, @1];
//    [gradientView.layer addSublayer:gradient];
    
    model = [[Model alloc] init];
}


- (void)viewWillLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)viewDidLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    SettingsViewCell *cell = nil;
    if(indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cropCell" forIndexPath:indexPath];
    }
    if(indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"landscapeCell" forIndexPath:indexPath];
        [cell.landscapeSwitch setOn:model.useLandscapeMode];
    }
    if(indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"hlsCell" forIndexPath:indexPath];
        [cell.hlsSwitch setOn:model.useHLSStreaming];
    }
    cell.delegate = self;
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        [self performSegueWithIdentifier:@"showCrop" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showCrop"])
    {
        ((CropViewController*)segue.destinationViewController).cropRect = model.cropRect;
        ((CropViewController*)segue.destinationViewController).delegate = self;
        ((CropViewController*)segue.destinationViewController).landscapeMode = model.useLandscapeMode;
    }
}


- (void)hlsModeChanged:(bool)useHLS {
    model.useHLSStreaming = useHLS;
    [model save];
}

- (void)landscapeModeChanged:(bool)landscape {
    model.useLandscapeMode = landscape;
    [model save];
}


- (void)cropAreaChanged:(CGRect)cropRect {
    model.cropRect = cropRect;
    [model save];
}


@end
