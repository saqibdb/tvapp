//
//  AppDelegate.m
//  ChromecastDisplay
//
//  Created by alex on 08/05/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleCast/GoogleCast.h>
#import "Model.h"
#ifndef NOIAP
#import "RMStore.h"
#import "RMAppReceipt.h"
#import "RMStoreAppReceiptVerificator.h"
#endif
#define INTERVAL_BETWEEN_REVIEWS_DAYS 14

@import StoreKit;
@import Security;

@interface AppDelegate () <GCKLoggerDelegate>
@end

@implementation AppDelegate

static NSString *const kReceiverAppID = @"CC43AD47";
static const BOOL kDebugLoggingEnabled = YES;

static NSString *const kBundleID = @"com.app9.transmit";
static NSString *const kNOIAPVersion = @"1.0";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    GCKDiscoveryCriteria *discoveryCriteria =
    [[GCKDiscoveryCriteria alloc] initWithApplicationID:kReceiverAppID];
    GCKCastOptions *options =
    [[GCKCastOptions alloc] initWithDiscoveryCriteria:discoveryCriteria];
    [GCKCastContext setSharedInstanceWithOptions:options];
    
    [[GCKLogger sharedInstance] setDelegate:self];
    
    
    Model* model = [[Model alloc] init];
    model.run++;
    
#ifdef NOIAP
    model.trialMode = false;
#endif
    
    [model save];
    if(model.run % 5 == 0 && ([NSDate date].timeIntervalSince1970 - model.lastTimeReviewRequested.timeIntervalSince1970) > 60*60*24*INTERVAL_BETWEEN_REVIEWS_DAYS) {
        [SKStoreReviewController requestReview];
        model.lastTimeReviewRequested = [NSDate date];
        [model save];
    }
    
    //self.window.tintColor = [UIColor whiteColor];

#ifndef NOIAP
    if(model.trialMode)
    {
        RMAppReceipt *receipt = [RMAppReceipt bundleReceipt];
        BOOL verified = [self verifyAppReceipt:receipt];
        if (!verified) {
            [[RMStore defaultStore] refreshReceiptOnSuccess:^{
                RMAppReceipt *refreshedReceipt = [RMAppReceipt bundleReceipt];
                if([self verifyAppReceipt:refreshedReceipt])
                {
                    if([self checkOriginalVersion:refreshedReceipt])
                    {
                        model.trialMode = false;
                        [model save];
                    }
                    NSLog(@"refreshed receipt validation OK");
                }
            } failure:^(NSError *error) {
                NSLog(@"receipt validation failed: %@", error);
            }];
        }
        else
        {
            NSLog(@"stored receipt validation OK");
            if([self checkOriginalVersion:receipt])
            {
                model.trialMode = false;
                [model save];
            }
        }
    }
#endif
    
    return YES;
}

#ifndef NOIAP

-(bool)checkOriginalVersion:(RMAppReceipt*)receipt
{
    bool result = [kNOIAPVersion isEqualToString:receipt.originalAppVersion];
    if(result) NSLog(@"NOIAP version was previously purchased");
    else NSLog(@"NOIAP version was not previously purchased");
    return result;
}

- (BOOL)verifyAppReceipt:(RMAppReceipt*)receipt
{
    if (!receipt) return NO;
    if (![receipt.bundleIdentifier isEqualToString:kBundleID]) return NO;
    if (![receipt verifyReceiptHash]) return NO;
    return YES;
}
#endif



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)logMessage:(NSString *)message fromFunction:(NSString *)function {
    if (kDebugLoggingEnabled) {
        // Send SDK's log messages directly to the console.
        NSLog(@"%@  %@", function, message);
    }
}

@end
