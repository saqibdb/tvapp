//
//  ViewController.m
//  ChromecastDisplay
//
//  Created by alex on 08/05/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "ViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <ConnectSDK/ConnectSDK.h>
#import "TVCell.h"
#import "CropViewController.h"
#import "WebPageViewController.h"
#import "Model.h"
#import "SubscribeViewController.h"
@import WebKit;



@interface ViewController ()<ConnectableDeviceDelegate, DiscoveryManagerDelegate, SubscribeViewControllerDelegate> {
    __weak IBOutlet UILabel *helpLabel;
    __weak IBOutlet UITableView *deviceTableView;
    __weak IBOutlet UIView *gradientView;
    __weak IBOutlet UIBarButtonItem *purchaseButton;
    
    int restartSessionCountdown;
    
    DiscoveryManager *_discoveryManager;
    ConnectableDevice *_device;
    MediaLaunchObject *_launchObject;
    
    SubscribeViewController* buyView;
    
    //CAGradientLayer* gradient;
    
    Model* model;
    
    ConnectableDevice *_postponedSelection;
};

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.backgroundColor = UIColor.clearColor;
    self.navigationController.navigationBar.translucent = true;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        
    // Do any additional setup after loading the view, typically from a nib.
/*    gradient = [[CAGradientLayer alloc] init];
    gradient.frame = gradientView.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:237/255.0 green:46/255.0 blue:99/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:205/255.0 green:0 blue:63/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:181/255.0 green:0 blue:35/255.0 alpha:1].CGColor];
    gradient.locations = @[@0, @0.5, @1];
    [gradientView.layer addSublayer:gradient];
*/
    restartSessionCountdown = 0;
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self processConnection];
    });
    
    CapabilityFilter *videoFilter = [CapabilityFilter filterWithCapabilities:@[kMediaPlayerPlayVideo]];
    
    _discoveryManager = [DiscoveryManager sharedManager];
    [_discoveryManager setCapabilityFilters:@[videoFilter]];
    [_discoveryManager startDiscovery];
    _discoveryManager.delegate = self;
    
    model = [[Model alloc] init];
   
    if(!model.trialMode) {
        self.navigationItem.rightBarButtonItem = nil;
    }
   
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

}

- (void)viewWillLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)viewDidLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)viewDidAppear:(BOOL)animated {
    model = [[Model alloc] init]; //reload data
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showFAQ"])
    {
        segue.destinationViewController.title = @"FAQ";
        ((WebPageViewController*)segue.destinationViewController).targetPath = [[NSBundle mainBundle] pathForResource:@"faq" ofType:@"html"];
    }
    if ([[segue identifier] isEqualToString:@"purchase"])
    {
        ((SubscribeViewController*)segue.destinationViewController).delegate = self;
    }
}

- (IBAction)showSettings:(id)sender {
    if(model.trialMode) {
        [self performSegueWithIdentifier:@"purchase" sender:nil];
    }
    else {
        [self performSegueWithIdentifier:@"showSettings" sender:nil];
    }
}

-(void)reloadDeviceList {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->deviceTableView reloadData];
    });
}

- (void) discoveryManager:(DiscoveryManager *)manager didFindDevice:(ConnectableDevice *)device {
    NSLog(@"new device found");
    [self reloadDeviceList];
}

- (void) discoveryManager:(DiscoveryManager *)manager didLoseDevice:(ConnectableDevice *)device {
    NSLog(@"device lost");
    [self reloadDeviceList];
}

- (void) discoveryManager:(DiscoveryManager *)manager didUpdateDevice:(ConnectableDevice *)device {
    NSLog(@"device updated");
    [self reloadDeviceList];
}

- (IBAction)tapRefreshDevices:(id)sender {
    [_discoveryManager startDiscovery];
    [self reloadDeviceList];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _discoveryManager.compatibleDevices.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TVCell* cell = [tableView dequeueReusableCellWithIdentifier:@"tvcell"];
    ConnectableDevice* device = [_discoveryManager.compatibleDevices.allValues objectAtIndex:indexPath.row];
    [cell.tvLabel setText:device.friendlyName];
    cell.screencastImage.hidden = device != _device;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ConnectableDevice* device = [_discoveryManager.compatibleDevices.allValues objectAtIndex:indexPath.row];
    if(device != nil) {
        if(model.trialMode) {
            _postponedSelection = device;
            [self performSegueWithIdentifier:@"purchase" sender:nil];
        }
        else
        {
            [self devicePickerDidSelectDevice:device];
        }
    }
}

-(void)performPostponedSelection {
    if(_postponedSelection != nil) {
        [self devicePickerDidSelectDevice:_postponedSelection];
        _postponedSelection = nil;
    }
}

- (void)devicePickerDidSelectDevice:(ConnectableDevice *)device
{
    restartSessionCountdown = 0;
    if (_device)
    {
        _device.delegate = nil;
        [_device disconnect];
        _device = nil;
    }
    _launchObject = NULL;
    _device = device;
    _device.delegate = self;
    [_device connect];
    
    [self reloadDeviceList];
}

-(void)startPlaybackForCurrentDevice {
    if(restartSessionCountdown <= 0)
    {
        restartSessionCountdown = 30;
        ConnectableDevice* device = _device;
        if(device != nil) {
            //https://www.hdpvrcapture.com/hdpvrcapture/samples/20090228_085119-H.264.m2ts
            
            
            NSURL *mediaURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:8080/ts.m2ts", [self getAddress]]];
            if(model.useHLSStreaming) {
                mediaURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:8080/Documents/root.m3u8", [self getAddress]]];
            }

            //NSURL *mediaURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.hdpvrcapture.com/hdpvrcapture/samples/20090228_085119-H.264.m2ts", [self getIPAddress]]];
            NSString *title = @"TVApp";
            NSString *description = @"TVApp live stream";
            NSString *mimeType = @"video/mp4"; // audio/* for audio files
            
            MediaInfo *mediaInfo = [[MediaInfo alloc] initWithURL:mediaURL mimeType:mimeType];
            mediaInfo.title = title;
            mediaInfo.description = description;
            
            [device.mediaPlayer playMediaWithMediaInfo:mediaInfo
                                             shouldLoop:YES
                                                success:
             ^(MediaLaunchObject *mediaLaunchObject) {
                 NSLog(@"play video success");
                 self->_launchObject = mediaLaunchObject;
             }
                                                failure:
             ^(NSError *error) {
                 NSLog(@"play video failure: %@", error.localizedDescription);
             }];
        }
    }
}

- (void)connectableDeviceReady:(ConnectableDevice *)device
{
    _device = device;
    if(_launchObject == NULL) {
        [self startPlaybackForCurrentDevice];
    }
}

- (void)connectableDeviceDisconnected:(ConnectableDevice *)device withError:(NSError *)error
{
    if(_device == device) device = NULL;
    _launchObject = NULL;
}


-(void)processConnection {
    if(restartSessionCountdown > 0) restartSessionCountdown--;
    bool bufferingNow = false;
    NSError* error;
    
//    NSString* ipAddress = [self getAddress];
//    if([ipAddress isEqualToString:@"error"]) {
//        [networkStatusLabel setText:@"Not connected to WiFi network"];
//    }
//    else {
//        [networkStatusLabel setText:[NSString stringWithFormat:@"Connected (%@)", ipAddress]];
//    }
    
    [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/ping"] encoding:NSUTF8StringEncoding error:&error];
    if(error != nil) {
        [helpLabel setText:@"Please start Screen Recorder from the Control Center."];
    }
    else {
        NSString* expired = [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/expired"] encoding:NSUTF8StringEncoding error:&error];

        NSLog(@"expired: %@", expired);
        
        if([expired isEqualToString:@"expired"]) {
            [helpLabel setText:@"Trial run is expired, you can recharge it below"];
            if(buyView == nil) {
                [self performSegueWithIdentifier:@"purchase" sender:nil];
            }
        }
        else{
            if(_device != nil)
            {
                [_device.mediaControl getPlayStateWithSuccess:^(MediaControlPlayState playState) {
                    NSLog(@"current device state: %i", playState);
                    if(playState != MediaControlPlayStatePlaying) [self startPlaybackForCurrentDevice];
                } failure:^(NSError *error) {
                    [self startPlaybackForCurrentDevice];
                    NSLog(@"current device state error: %@", error);
                }];
                [helpLabel setText:@"Connection is ON now, you can change your settings if needed."];
            }
            else {
                //self.navigationItem.leftBarButtonItem = nil;
                [helpLabel setText:@"Your TV must be connected to the same network as your mobile device."];
            }
        }
    }
    [self updateTransform];
    [self sendTrialMode:model.trialMode];
    
    if(model.trialMode)
    {
        model = [[Model alloc] init]; //reload state
        if(!model.trialMode) [self purchased];
    }
   
    //croppingLabel.text = CGRectEqualToRect(_cropRect, CGRectMake(0, 0, 1, 1)) ? @"Cropping disabled" : @"Cropping enabled";
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((bufferingNow ? 10 : 1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self processConnection];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

-(void)continueTrialSelected {
    [self rechargeTrial];
    NSLog(@"continue trial selected");
    [self performPostponedSelection];
}

-(void)purchased {
    [self sendTrialMode:false];
    model.trialMode = false;
    [model save];
    
    [self sendTrialMode:false];
    
    self.navigationItem.rightBarButtonItem = nil;

    [self performPostponedSelection];
    NSLog(@"product purchased");
}


-(void)sendTrialMode:(bool)trial {
    NSError* error;
    if(trial) {
        [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/trial"] encoding:NSUTF8StringEncoding error:&error];
    }
    else {
        [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/purchase"] encoding:NSUTF8StringEncoding error:&error];
    }
}

-(void)rechargeTrial {
    NSError* error;
    [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/recharge"] encoding:NSUTF8StringEncoding error:&error];
}


-(void)sendTransformMessage:(bool)landscape {
    NSError* error;
    if(landscape) {
        [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/landscape"] encoding:NSUTF8StringEncoding error:&error];
    }
    else {
        [NSString stringWithContentsOfURL:[NSURL URLWithString: @"http://127.0.0.1:8080/portrait"] encoding:NSUTF8StringEncoding error:&error];
    }
}

-(void)sendCropMessage:(CGRect)cropRect {
    NSError* error;
    NSString* url = [NSString stringWithFormat:@"http://127.0.0.1:8080/crop?crop_left=%f&crop_top=%f&crop_width=%f&crop_height=%f",
                     cropRect.origin.x,
                     cropRect.origin.y,
                     cropRect.size.width,
                     cropRect.size.height];
    [NSString stringWithContentsOfURL:[NSURL URLWithString: url] encoding:NSUTF8StringEncoding error:&error];
}

-(void)updateTransform {
    [self sendTransformMessage:model.useLandscapeMode];
    [self sendCropMessage:model.cropRect];
}

- (IBAction)showPrivacyPolicy:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://videoapp9.weebly.com/privacy-policy.html"] options:@{} completionHandler:nil];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://videoapp9.weebly.com/privacy-policy.html"]];
}

@end
