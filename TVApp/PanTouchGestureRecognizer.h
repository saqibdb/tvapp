//
//  PanTouchGestureRecognizer.h
//  TVS
//
//  Created by alex on 15/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PanTouchGestureRecognizer : UIPanGestureRecognizer

- (id)initWithTarget:(nullable id)target action:(nullable SEL)action touchDown:(nullable SEL)touchDownAction;

@end
