//
//  TVCell.m
//
//  Created by alex on 05/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "TVCell.h"

@implementation TVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
