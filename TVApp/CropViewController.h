//
//  CropViewController.h
//  TVS
//
//  Created by alex on 11/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CropViewDelegate
-(void)cropAreaChanged:(CGRect)cropRect;
@end


@interface CropViewController : UIViewController
@property (nonatomic) bool landscapeMode;
@property (nonatomic) id<CropViewDelegate> delegate;
@property (nonatomic) CGRect cropRect;

@end
