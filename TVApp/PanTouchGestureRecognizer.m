//
//  PanTouchGestureRecognizer.m
//  TVS
//
//  Created by alex on 15/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "PanTouchGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

@interface PanTouchGestureRecognizer(){
    SEL touchDownSelector;
    id touchDownTarget;
}
@end;

@implementation PanTouchGestureRecognizer

- (instancetype)initWithTarget:(nullable id)target action:(nullable SEL)action touchDown:(nullable SEL)touchDownAction {
    self = [super initWithTarget:target action:action];
    if(self)
    {
        touchDownSelector = touchDownAction;
        touchDownTarget = target;
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [touchDownTarget performSelector:touchDownSelector withObject:self afterDelay:0];
}


@end
