//
//  CropView.h
//  TVS
//
//  Created by alex on 15/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CropView : UIView

@property (nonatomic) CGRect cropRect;
@property (nonatomic) bool landscapeMode;

-(void)processTouchDown:(CGPoint)point;
-(void)processTranslation:(CGPoint)translation;

@end
