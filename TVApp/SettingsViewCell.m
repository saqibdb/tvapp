//
//  SettingsViewCell.m
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "SettingsViewCell.h"

@implementation SettingsViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)landscapeModeChanged:(id)sender {
    if(_delegate != nil) [_delegate landscapeModeChanged:_landscapeSwitch.isOn];
}

- (IBAction)hlsModeChanged:(id)sender {
    if(_delegate != nil) [_delegate hlsModeChanged:_hlsSwitch.isOn];
}

@end
