//
//  SubscribeViewController.m
//  TVS
//
//  Created by alex on 24/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "SubscribeViewController.h"
#import "Model.h"
#import <StoreKit/StoreKit.h>
#define kProductID @"com.app9.transmit.subscription"

@interface SubscribeViewController () <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    CAGradientLayer* gradient;
    Model* model;
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    __weak IBOutlet UIButton *purchaseButton;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}
@property (weak, nonatomic) IBOutlet UIView *gradientView;

@end

@implementation SubscribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  
/*    gradient = [[CAGradientLayer alloc] init];
    gradient.frame = self.gradientView.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0 green:106/255.0 blue:246/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:42/255.0 green:0 blue:161/255.0 alpha:1].CGColor];
    gradient.locations = @[@0, @1];
*/
    [self.gradientView.layer addSublayer:gradient];
    
    model = [[Model alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    if(!model.trialMode) [self dismissViewControllerAnimated:false completion:nil]; //[self performSegueWithIdentifier:@"showStreamer" sender:self];
    else
    {
        purchaseButton.hidden = YES;
        [self fetchAvailableProducts];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
//    gradient.frame = self.gradientView.bounds;
}

- (void)viewWillLayoutSubviews {
//    gradient.frame = self.gradientView.bounds;
}

- (IBAction)buyNow:(id)sender {
    [self purchaseMyProduct:[validProducts objectAtIndex:0]];
    purchaseButton.enabled = NO;
}

- (IBAction)restorePurchases:(id)sender {
    NSLog(@"restore request");
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (IBAction)startTrial:(id)sender {
    if([NSDate date].timeIntervalSince1970 - model.trialStartDate.timeIntervalSince1970 > 60*60*24*3) {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Expired"                                                                      message:@"Your trial version is expired, buy full version to continue"                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [defaultAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        if([NSDate date].timeIntervalSince1970 - model.trialStartDate.timeIntervalSince1970 < 60*60) {
            model.trialStartDate = [NSDate date];
            [model save];
        }
        
        if(self.delegate != nil) [self.delegate continueTrialSelected];
        
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

-(void)fetchAvailableProducts {
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:kProductID,nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    
}

- (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %i", (int)queue.transactions.count);
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        NSString *productID = transaction.payment.productIdentifier;
        if([productID isEqualToString:kProductID]) [self purchaseRestored];
    }
}

- (void)purchaseMyProduct:(SKProduct*)product {
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Not available"                                                                      message:@"Purchases are disabled in your device"                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)purchaseDone {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""                                                                      message:@"Purchase is completed successfully"                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              model.trialMode = false;
                                                              [model save];
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [self dismissViewControllerAnimated:true completion:nil];
                                                              });
                                                              if(self.delegate != nil) [self.delegate purchased];
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)purchaseRestored {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""                                                                      message:@"Purchase is successfully restored"                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              model.trialMode = false;
                                                              [model save];
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [self dismissViewControllerAnimated:true completion:nil];
                                                              });
                                                              if(self.delegate != nil) [self.delegate purchased];
                                                          }];

    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)purchaseFailed {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""                                                                      message:@"Purchase failed, please try again later"                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
                
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kProductID]) {
                    NSLog(@"Purchased ");

                    
                    [self purchaseDone];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self purchaseDone];
                break;
                
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
          //      [self purchaseFailed];
                break;
            case SKPaymentTransactionStateDeferred:
         //       [self purchaseFailed];
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response {
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    
    bool productFound = false;
    if (count>0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"product found: %@", validProduct.productIdentifier);
        if ([validProduct.productIdentifier
             isEqualToString:kProductID]) {
            productFound = true;
        }
    }
    else {
        NSLog(@"no products");
    }
    
    purchaseButton.hidden = !productFound;
    if(!productFound) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Not available"                                                                      message:@"No products to purchase"                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [activityIndicator stopAnimating];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
