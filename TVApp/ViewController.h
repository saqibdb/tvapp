//
//  ViewController.h
//  ChromecastDisplay
//
//  Created by alex on 08/05/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@end

