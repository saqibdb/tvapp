//
//  WebPageViewController.h
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebPageViewController : UIViewController

@property (nonatomic) NSString* targetPath;

@end
