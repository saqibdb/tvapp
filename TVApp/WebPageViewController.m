//
//  WebPageViewController.m
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "WebPageViewController.h"
@import WebKit;

@interface WebPageViewController () {
    __weak IBOutlet UIView *gradientView;
    CAGradientLayer* gradient;
}
@property (weak, nonatomic) IBOutlet WKWebView *wkWebView;

@end

@implementation WebPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    gradient = [[CAGradientLayer alloc] init];
//    gradient.frame = gradientView.bounds;
//    gradient.colors = @[(id)[UIColor colorWithRed:237/255.0 green:46/255.0 blue:99/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:205/255.0 green:0 blue:63/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:181/255.0 green:0 blue:35/255.0 alpha:1].CGColor];
//    gradient.locations = @[@0, @0.5, @1];
//    [gradientView.layer addSublayer:gradient];
}

- (void)viewDidAppear:(BOOL)animated {
    NSURL *targetURL = [NSURL fileURLWithPath:self.targetPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [self.wkWebView loadRequest:request];
}


- (void)viewWillLayoutSubviews {
//    gradient.frame = gradientView.bounds;
}

- (void)viewDidLayoutSubviews {
//    gradient.frame = gradientView.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
