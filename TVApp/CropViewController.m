//
//  CropViewController.m
//  TVS
//
//  Created by alex on 11/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "CropViewController.h"
#import "CropView.h"
#import "PanTouchGestureRecognizer.h"

@interface CropViewController () {
    UIPanGestureRecognizer *panRecognizer;
    __weak IBOutlet UIView *gradientView;
    CAGradientLayer* gradient;
}

@property (weak, nonatomic) IBOutlet CropView *cropView;
@property (weak, nonatomic) NSLayoutConstraint *aspectRatioConstraint;

@end

@implementation CropViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    panRecognizer = [[PanTouchGestureRecognizer alloc] initWithTarget:self action:@selector(cropDrag:) touchDown:@selector(cropTap:)];
    [_cropView addGestureRecognizer:panRecognizer];
    
    bool ipad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);

    
    if(self.landscapeMode) {
        _aspectRatioConstraint = [NSLayoutConstraint
                                  constraintWithItem:self.cropView
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.cropView
                                  attribute:NSLayoutAttributeHeight
                                  multiplier: (ipad ? 932.0/704.0 : 951.0/544.0)
                                  constant:0];
        [self.cropView addConstraint:_aspectRatioConstraint];
    }
    else {
        _aspectRatioConstraint = [NSLayoutConstraint
                                  constraintWithItem:self.cropView
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.cropView
                                  attribute:NSLayoutAttributeHeight
                                  multiplier: (ipad) ? 704.0/932 : 544.0/951.0
                                  constant:0];
        [self.cropView addConstraint:_aspectRatioConstraint];
    }
//    gradient = [[CAGradientLayer alloc] init];
//    gradient.frame = gradientView.bounds;
//    gradient.colors = @[(id)[UIColor colorWithRed:237/255.0 green:46/255.0 blue:99/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:205/255.0 green:0 blue:63/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:181/255.0 green:0 blue:35/255.0 alpha:1].CGColor];
//    gradient.locations = @[@0, @0.5, @1];
//    [gradientView.layer addSublayer:gradient];
}


- (void)viewWillLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
    if(_delegate) [_delegate cropAreaChanged:_cropView.cropRect];
}

- (void)viewWillDisappear:(BOOL)animated {
    if(_delegate) [_delegate cropAreaChanged:_cropView.cropRect];
}

- (IBAction)disable:(id)sender {
    _cropRect = CGRectMake(0, 0, 1, 1);
    _cropView.cropRect = CGRectMake(0, 0, 1, 1);
    [_cropView setNeedsDisplay];
}

- (void)cropDrag:(UIPanGestureRecognizer*)sender {
    NSLog(@"dx: %f, dy: %f", [sender translationInView:_cropView].x,     [sender translationInView:_cropView].y);
    [self.cropView processTranslation:[sender translationInView:_cropView]];
}

- (void)cropTap:(UITapGestureRecognizer*)sender {
    NSLog(@"tapX: %f, tapY: %f", [sender locationInView:_cropView].x, [sender locationInView:_cropView].y);
    [self.cropView processTouchDown:[sender locationInView:_cropView]];
}

- (void)setLandscapeMode:(bool)landscapeMode {
    _landscapeMode = landscapeMode;
    if(self.cropView != nil) {
        self.cropView.landscapeMode = landscapeMode;
        if(_aspectRatioConstraint != nil) {
            [self.cropView removeConstraint:_aspectRatioConstraint];
            _aspectRatioConstraint = nil;
        }
    }
}



- (void)viewDidLayoutSubviews {
    if(self.cropView != nil) {
        self.cropView.landscapeMode = _landscapeMode;
        self.cropView.cropRect = _cropRect;
    }
    //gradient.frame = gradientView.bounds;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
