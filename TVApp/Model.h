//
//  Model.h
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Model : NSObject

-(id)init;

@property (nonatomic) CGRect cropRect;
@property (nonatomic) bool useHLSStreaming;
@property (nonatomic) bool useLandscapeMode;
@property (nonatomic) bool trialMode;
@property (nonatomic) unsigned long run;
@property (nonatomic) NSDate* trialStartDate;
@property (nonatomic) NSDate* lastTimeReviewRequested;

-(void) save;

@end
