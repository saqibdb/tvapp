//
//  CropView.m
//  TVS
//
//  Created by alex on 15/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "CropView.h"
typedef enum
{
    CornerTopLeft = 0,
    CornerTopRight = 1,
    CornerBottomLeft = 2,
    CornerBottomRight = 3,
    CornerCenter = 4,
} Corner;


@interface CropView(){
    Corner selectedCorner;
    CGRect touchDownRect;
    CGRect transformScale;
}
@end

@implementation CropView


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.cropRect = CGRectMake(0, 0, 1, 1);
        transformScale = CGRectMake(0, 0, 1, 1);
        selectedCorner = 0;
    }
    return self;
}

-(CGRect)getAbsPosRect {
    return CGRectMake((_cropRect.origin.x * transformScale.size.width + transformScale.origin.x) * self.frame.size.width,
                    (_cropRect.origin.y * transformScale.size.height + transformScale.origin.y) * self.frame.size.height,
                    _cropRect.size.width * self.frame.size.width * transformScale.size.width,
                    _cropRect.size.height * self.frame.size.height * transformScale.size.height);
}

-(CGPoint)toCropCoordinates:(CGPoint)point {
    if(self.frame.size.width > 0 && self.frame.size.height > 0)
    {
        return CGPointMake(point.x / (self.frame.size.width * transformScale.size.width), point.y / (self.frame.size.height * transformScale.size.height));
    }
    else return CGPointZero;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    UIBezierPath* linePath = [[UIBezierPath alloc] init];

/*    horizontalLinePath.move(to: CGPoint(x:-frame.width/2, y:0));
    horizontalLinePath.addLine(to: CGPoint(x:frame.width/2, y:0));
    horizontalLinePath.lineWidth = 3;
    UIColor.white.set();
    horizontalLinePath.apply(CGAffineTransform.init(translationX: self.frame.width/2, y: frame.height/2));
    horizontalLinePath.stroke();*/
    
    int cornerLineWidth = 4;
    int cornerSize = 15;
    
    CGRect absPos = [self getAbsPosRect];
    
    //top left
    linePath = [[UIBezierPath alloc] init];
    [linePath moveToPoint:CGPointMake(absPos.origin.x, absPos.origin.y + cornerSize)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x, absPos.origin.y)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x + cornerSize, absPos.origin.y)];
    linePath.lineWidth = cornerLineWidth;
    [UIColor.whiteColor set];
    [linePath stroke];
    
    
    //top right
    linePath = [[UIBezierPath alloc] init];
    [linePath moveToPoint:CGPointMake(absPos.origin.x + absPos.size.width - cornerSize, absPos.origin.y)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y + cornerSize)];
    linePath.lineWidth = cornerLineWidth;
    [UIColor.whiteColor set];
    [linePath stroke];
    
    //bottom left
    linePath = [[UIBezierPath alloc] init];
    [linePath moveToPoint:CGPointMake(absPos.origin.x, absPos.origin.y + absPos.size.height - cornerSize)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x, absPos.origin.y + absPos.size.height)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x + cornerSize, absPos.origin.y + absPos.size.height)];
    linePath.lineWidth = cornerLineWidth;
    [UIColor.whiteColor set];
    [linePath stroke];
    
    //bottom right
    linePath = [[UIBezierPath alloc] init];
    [linePath moveToPoint:CGPointMake(absPos.origin.x + absPos.size.width - cornerSize, absPos.origin.y + absPos.size.height)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y + absPos.size.height)];
    [linePath addLineToPoint:CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y + absPos.size.height - cornerSize)];
    linePath.lineWidth = cornerLineWidth;
    [UIColor.whiteColor set];
    [linePath stroke];

    double gridLineWidth = 0.5;
    int gridLineCount = 4;
    
    //horizontal grid
    for(int i = 0; i < gridLineCount; i++)
    {
        linePath = [[UIBezierPath alloc] init];
        [linePath moveToPoint:CGPointMake(absPos.origin.x, absPos.origin.y + (i * absPos.size.height)/(double)(gridLineCount - 1))];
        [linePath addLineToPoint:CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y + (i * absPos.size.height)/(double)(gridLineCount - 1))];
        linePath.lineWidth = gridLineWidth;
        [[UIColor colorWithWhite:1 alpha:0.5] set];
        [linePath stroke];
    }
    
    //vertical grid
    for(int i = 0; i < gridLineCount; i++)
    {
        linePath = [[UIBezierPath alloc] init];
        [linePath moveToPoint:CGPointMake(absPos.origin.x + (i * absPos.size.width)/(double)(gridLineCount - 1), absPos.origin.y)];
        [linePath addLineToPoint:CGPointMake(absPos.origin.x + (i * absPos.size.width)/(double)(gridLineCount - 1), absPos.origin.y + absPos.size.height)];
        linePath.lineWidth = gridLineWidth;
        [[UIColor colorWithWhite:1 alpha:0.5] set];
        [linePath stroke];
    }
}

-(CGPoint)coordinateForCorner:(Corner)corner {
    CGRect absPos = [self getAbsPosRect];
    switch(corner)
    {
        case CornerTopLeft:
            return CGPointMake(absPos.origin.x, absPos.origin.y);
            break;
        case CornerTopRight:
            return CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y);
            break;
        case CornerBottomLeft:
            return CGPointMake(absPos.origin.x, absPos.origin.y + absPos.size.height);
            break;
        case CornerBottomRight:
            return CGPointMake(absPos.origin.x + absPos.size.width, absPos.origin.y + absPos.size.height);
            break;
        case CornerCenter:
            return CGPointMake(absPos.origin.x + absPos.size.width / 2, absPos.origin.y + absPos.size.height / 2);
            break;
    }
}

-(Corner)getNearestCorner:(CGPoint)toPoint {
    Corner result = CornerTopLeft;
    float minDistance = INFINITY;
    for(int i = 0; i < 5; i++)
    {
        CGPoint cornerPoint = [self coordinateForCorner:i];
        CGVector vec = CGVectorMake(toPoint.x - cornerPoint.x, toPoint.y - cornerPoint.y);
        float distance = vec.dx*vec.dx + vec.dy*vec.dy;
        if(distance < minDistance)
        {
            result = i;
            minDistance = distance;
        }
    }
    return result;
}

-(void)processTouchDown:(CGPoint)point {
    selectedCorner = [self getNearestCorner:point];
    touchDownRect = _cropRect;
    NSLog(@"selected corner: %i", selectedCorner);
}



-(void)translateCorner:(Corner)corner withTranslation:(CGPoint)translation {
    float minSize = 0.1;
    CGPoint vec = [self toCropCoordinates:translation];
    CGRect newRect = _cropRect;
    switch(corner) {
        case CornerTopLeft:
            if(touchDownRect.origin.x + vec.x < 0) vec.x = -touchDownRect.origin.x;
            if(touchDownRect.size.width - vec.x < minSize) vec.x = touchDownRect.size.width - minSize;
            if(touchDownRect.origin.y + vec.y < 0) vec.y = -touchDownRect.origin.y;
            if(touchDownRect.size.height - vec.y < minSize) vec.y = touchDownRect.size.height - minSize;

            newRect.origin.x = touchDownRect.origin.x + vec.x;
            newRect.origin.y = touchDownRect.origin.y + vec.y;
            newRect.size.width = touchDownRect.size.width - vec.x;
            newRect.size.height = touchDownRect.size.height - vec.y;
            break;
        case CornerTopRight:
            if(touchDownRect.size.width + vec.x + touchDownRect.origin.x > 1) vec.x = 1 - touchDownRect.size.width - touchDownRect.origin.x;
            if(touchDownRect.size.width + vec.x < minSize) vec.x = minSize - touchDownRect.size.width;
            if(touchDownRect.origin.y + vec.y < 0) vec.y = -touchDownRect.origin.y;
            if(touchDownRect.size.height - vec.y < minSize) vec.y = touchDownRect.size.height - minSize;
            newRect.origin.y = touchDownRect.origin.y + vec.y;
            newRect.size.width = touchDownRect.size.width + vec.x;
            newRect.size.height = touchDownRect.size.height - vec.y;
            break;
        case CornerBottomLeft:
            if(touchDownRect.origin.x + vec.x < 0) vec.x = -touchDownRect.origin.x;
            if(touchDownRect.size.width - vec.x < minSize) vec.x = touchDownRect.size.width - minSize;
            if(touchDownRect.size.height + vec.y + touchDownRect.origin.y > 1) vec.y = 1 - touchDownRect.size.height - touchDownRect.origin.y;
            if(touchDownRect.size.height + vec.y < minSize) vec.y = minSize - touchDownRect.size.height;
            newRect.origin.x = touchDownRect.origin.x + vec.x;
            newRect.size.width = touchDownRect.size.width - vec.x;
            newRect.size.height = touchDownRect.size.height + vec.y;
            break;
        case CornerBottomRight:
            if(touchDownRect.size.width + vec.x + touchDownRect.origin.x > 1) vec.x = 1 - touchDownRect.size.width - touchDownRect.origin.x;
            if(touchDownRect.size.width + vec.x < minSize) vec.x = minSize - touchDownRect.size.width;
            if(touchDownRect.size.height + vec.y + touchDownRect.origin.y > 1) vec.y = 1 - touchDownRect.size.height - touchDownRect.origin.y;
            if(touchDownRect.size.height + vec.y < minSize) vec.y = minSize - touchDownRect.size.height;
            newRect.size.width = touchDownRect.size.width + vec.x;
            newRect.size.height = touchDownRect.size.height + vec.y;
            break;
        case CornerCenter:
            if(touchDownRect.origin.x + vec.x < 0) vec.x = -touchDownRect.origin.x;
            if(touchDownRect.size.width + touchDownRect.origin.x + vec.x > 1) vec.x = 1 - touchDownRect.size.width - touchDownRect.origin.x;
            if(touchDownRect.origin.y + vec.y < 0) vec.y = -touchDownRect.origin.y;
            if(touchDownRect.size.height + touchDownRect.origin.y + vec.y > 1) vec.y = 1 - touchDownRect.size.height - touchDownRect.origin.y;
            
            newRect.origin.x = touchDownRect.origin.x + vec.x;
            newRect.origin.y = touchDownRect.origin.y + vec.y;
            break;
    }
    
    /*if(newRect.origin.x <= 0 ||
       newRect.origin.x + newRect.size.width > 1 ||
       newRect.size.width < minSize)
    {
        newRect.origin.x = _cropRect.origin.x;
        newRect.size.width = _cropRect.size.width;
    }
    if(newRect.origin.y <= 0 ||
       newRect.origin.y + newRect.size.height > 1 ||
       newRect.size.height < minSize)
    {
        newRect.origin.y = _cropRect.origin.y;
        newRect.size.height = _cropRect.size.height;
    }*/
    
    _cropRect = newRect;
    
    [self setNeedsDisplay];
}

-(void)processTranslation:(CGPoint)translation {
    [self translateCorner:selectedCorner withTranslation:translation];
}

- (void)setLandscapeMode:(bool)landscapeMode {
    _landscapeMode = landscapeMode;
    [self layoutSubviews];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    UIGraphicsBeginImageContext(self.frame.size);
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [[UIImage imageNamed:_landscapeMode ? @"landscapeTablet" : @"portraitTablet"] drawInRect:self.bounds];
        if(_landscapeMode) {
            transformScale = CGRectMake(0.116, 0.105, 0.77, 0.767);
        }
        else {
            transformScale = CGRectMake(0.117, 0.106, 0.767, 0.77);
        }
    }
    else
    {
        [[UIImage imageNamed:_landscapeMode ? @"landscapePhone" : @"portraitPhone"] drawInRect:self.bounds];
        if(_landscapeMode) {
            transformScale = CGRectMake(0.163, 0.126, 0.675, 0.66);
        }
        else {
            transformScale = CGRectMake(0.17, 0.138, 0.66, 0.675);
        }
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.backgroundColor = [UIColor colorWithPatternImage:image];
    
}

- (void)setCropRect:(CGRect)cropRect {
    _cropRect = cropRect;
    NSLog(@"crop area set in view: %fx%f, %fx%f", cropRect.origin.x, cropRect.origin.y, cropRect.size.width, cropRect.size.height);
}

@end
