//
//  SubscribeViewController.h
//  TVS
//
//  Created by alex on 24/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SubscribeViewControllerDelegate
-(void)continueTrialSelected;
-(void)purchased;
@end


@interface SubscribeViewController : UIViewController

@property (nonatomic, weak) id<SubscribeViewControllerDelegate> delegate;

@end
