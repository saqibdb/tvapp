//
//  Model.m
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "Model.h"

@implementation Model

- (instancetype)init
{
    self = [super init];
    if (self) {
        double cropLeft = [[NSUserDefaults standardUserDefaults] doubleForKey:@"cropLeft"];
        double cropTop = [[NSUserDefaults standardUserDefaults] doubleForKey:@"cropTop"];
        double cropWidth = [[NSUserDefaults standardUserDefaults] doubleForKey:@"cropWidth"];
        double cropHeight = [[NSUserDefaults standardUserDefaults] doubleForKey:@"cropHeight"];
        if(cropWidth < 0.01) cropWidth = 1;
        if(cropHeight < 0.01) cropHeight = 1;

        _cropRect = CGRectMake(cropLeft, cropTop, cropWidth, cropHeight);
        _useHLSStreaming = [[NSUserDefaults standardUserDefaults] boolForKey:@"useHLS"];
        _useLandscapeMode = [[NSUserDefaults standardUserDefaults] boolForKey:@"useLandscape"];
        _trialMode = ![[NSUserDefaults standardUserDefaults] boolForKey:@"purchased"];
        _run = [[NSUserDefaults standardUserDefaults] integerForKey:@"run"];
        
        double startDate = [[NSUserDefaults standardUserDefaults] doubleForKey:@"trialStartDate"];
        if(startDate > 1) {
            _trialStartDate = [NSDate dateWithTimeIntervalSince1970:startDate];
        }
        else _trialStartDate = [NSDate date];
        
        double lastRequestDate = [[NSUserDefaults standardUserDefaults] doubleForKey:@"lastTimeReviewRequested"];
        _lastTimeReviewRequested = [NSDate dateWithTimeIntervalSince1970:lastRequestDate];
    }
    return self;
}

- (void)save {
    [[NSUserDefaults standardUserDefaults] setDouble:_cropRect.origin.x forKey:@"cropLeft"];
    [[NSUserDefaults standardUserDefaults] setDouble:_cropRect.origin.y forKey:@"cropTop"];
    [[NSUserDefaults standardUserDefaults] setDouble:_cropRect.size.width forKey:@"cropWidth"];
    [[NSUserDefaults standardUserDefaults] setDouble:_cropRect.size.height forKey:@"cropHeight"];
    [[NSUserDefaults standardUserDefaults] setBool:_useHLSStreaming forKey:@"useHLS"];
    [[NSUserDefaults standardUserDefaults] setBool:_useLandscapeMode forKey:@"useLandscape"];
    [[NSUserDefaults standardUserDefaults] setBool:!_trialMode forKey:@"purchased"];
    [[NSUserDefaults standardUserDefaults] setInteger:_run forKey:@"run"];
    
    [[NSUserDefaults standardUserDefaults] setDouble:[_trialStartDate timeIntervalSince1970] forKey:@"trialStartDate"];

    [[NSUserDefaults standardUserDefaults] setDouble:[_lastTimeReviewRequested timeIntervalSince1970] forKey:@"lastTimeReviewRequested"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
