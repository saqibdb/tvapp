//
//  HelpViewController.m
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "HelpViewController.h"
#import "HelpEntryViewCell.h"
#import "WebPageViewController.h"

@interface HelpViewController () <UITableViewDataSource, UITableViewDelegate> {
    __weak IBOutlet UIView *gradientView;
    CAGradientLayer* gradient;
}

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    gradient = [[CAGradientLayer alloc] init];
//    gradient.frame = gradientView.bounds;
//    gradient.colors = @[(id)[UIColor colorWithRed:237/255.0 green:46/255.0 blue:99/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:205/255.0 green:0 blue:63/255.0 alpha:1].CGColor,
//                        (id)[UIColor colorWithRed:181/255.0 green:0 blue:35/255.0 alpha:1].CGColor];
//    gradient.locations = @[@0, @0.5, @1];
//    [gradientView.layer addSublayer:gradient];
}


- (void)viewWillLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)viewDidLayoutSubviews {
    //gradient.frame = gradientView.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HelpEntryViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"helpEntry" forIndexPath:indexPath];
    if(indexPath.row == 0) {
        cell.label.text = @"FAQ";
        cell.icon.image = [UIImage imageNamed:@"help"];
    }
    if(indexPath.row == 1) {
        cell.label.text = @"Step-by-Step Guide";
        cell.icon.image = [UIImage imageNamed:@"puzzled"];
    }
    if(indexPath.row == 2) {
        cell.label.text = @"Privacy Policy";
        cell.icon.image = [UIImage imageNamed:@"privacy"];
    }
    return cell;
}


/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        [self performSegueWithIdentifier:@"showFAQ" sender:self];
    }
    if(indexPath.row == 1) {
        [self performSegueWithIdentifier:@"showHowTo" sender:self];
    }
    if(indexPath.row == 2) {
        [self showPrivacyPolicy];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showFAQ"])
    {
        segue.destinationViewController.title = @"FAQ";
        ((WebPageViewController*)segue.destinationViewController).targetPath = [[NSBundle mainBundle] pathForResource:@"faq" ofType:@"html"];
    }
    if ([[segue identifier] isEqualToString:@"showHowTo"])
    {
        segue.destinationViewController.title = @"Step-by-Step Guide";
        ((WebPageViewController*)segue.destinationViewController).targetPath = [[NSBundle mainBundle] pathForResource:@"howto" ofType:@"html"];
    }
}


- (void)showPrivacyPolicy {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://videoapp9.weebly.com/privacy-policy.html"] options:@{} completionHandler:nil];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://videoapp9.weebly.com/privacy-policy.html"]];
}

@end
