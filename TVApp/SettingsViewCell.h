//
//  SettingsViewCell.h
//  TVS
//
//  Created by alex on 25/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsViewCellDelegate
-(void)landscapeModeChanged:(bool)landscape;
-(void)hlsModeChanged:(bool)useHLS;
@end

@interface SettingsViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *landscapeSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *hlsSwitch;
@property (weak, nonatomic) id<SettingsViewCellDelegate> delegate;
@end
