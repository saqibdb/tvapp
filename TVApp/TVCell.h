//
//  TVCell.h
//
//  Created by alex on 05/10/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *tvLabel;
@property (weak, nonatomic) IBOutlet UIImageView *screencastImage;

@end
