//
//  NSObject+Buffer.m
//
//  Created by alex on 11/09/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import "Buffer.h"
#include "Media/CircularBufferStatic.h"
#include <memory>
@interface Buffer() {
    std::unique_ptr<CircularBufferStatic> m_buffer;
}
@end

@implementation Buffer
-(id) initWithSize:(size_t)size {
    self = [self init];
    if(self) {
        try
        {
            m_buffer = std::make_unique<CircularBufferStatic>(size);
        }
        catch(...)
        {
            return nil;
        }
    }
    return self;
}

-(size_t)GetMaxWriteSize {
    return m_buffer->GetMaxWriteSize();
}

-(size_t)GetWaterLevel {
    return m_buffer->GetWaterLevel();
}

-(bool)PutData:(uint8_t*)data size:(size_t)size {
    return m_buffer->Write(data, size);
}

-(size_t)ReadData:(uint8_t*)data maxSize:(size_t)size {
    return m_buffer->Read(data, size);
}

-(void) Clear {
    m_buffer->Clear();
}

@end
