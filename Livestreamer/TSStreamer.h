//
//  HLSMuxer.h
//  MultiStreamer
//
//  Created by alex on 27/04/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VideoToolbox/VideoToolbox.h>
#import "Buffer.h"

@interface TSStreamer : NSObject

-(id) init;
-(void) putVideoSample:(CMSampleBufferRef)sampleBuffer;
-(void) putAudioSample:(uint8_t*)data size:(size_t)size pts:(CMTime)pts;
//-(uint8_t) fillTSBuffer:(uint8_t*)data size:(size_t)size;
-(void) SubscribeBuffer:(Buffer*)buffer;
-(void) UnsubscribeBuffer:(Buffer*)buffer;
-(void) flush;

@end

