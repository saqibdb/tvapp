//
//  NSObject+Buffer.h
//
//  Created by alex on 11/09/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Buffer : NSObject
-(id) initWithSize:(size_t)size;
-(size_t)GetMaxWriteSize;
-(size_t)GetWaterLevel;
-(bool)PutData:(uint8_t*)data size:(size_t)size;
-(size_t)ReadData:(uint8_t*)data maxSize:(size_t)size;
-(void) Clear;

@property bool waitingForRandomAccessPoint;
@end
