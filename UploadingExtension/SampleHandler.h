//
//  SampleHandler.h
//  UploadingExtension
//
//  Created by alex on 08/05/2018.
//  Copyright © 2018 alex. All rights reserved.
//

#import <ReplayKit/ReplayKit.h>
#import "Buffer.h"

@interface SampleHandler : RPBroadcastSampleHandler

-(void) SubscribeBuffer:(Buffer*)buffer;
-(void) UnsubscribeBuffer:(Buffer*)buffer;

@property bool broadcastActive;
@end
