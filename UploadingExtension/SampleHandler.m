//
//  SampleHandler.m
//  BroadcastExtension
//
//  Created by alex on 12/04/2018.
//  Copyright © 2018 alex. All rights reserved.
//


#import "SampleHandler.h"
#import "MongooseDaemon.h"
#import "TSStreamer.h"
#import "HLSMuxer.h"
#import "H264Encoder.h"
#import "FDKAACEncoder.h"
#import "InfiniteAudio.h"
#import <CoreImage/CoreImage.h>
#import <Accelerate/Accelerate.h>
#include <libkern/OSAtomic.h>

#define ENCODER_HEIGHT 960
#define ENCODER_WIDTH 540
#define ENCODER_WIDE_WIDTH 720
#define ENCODER_BITRATE 5000

#define TRIAL_DURATION_SECONDS 60

@interface PixelBuffer: NSObject
@property (nonatomic) CVPixelBufferRef pixels;
@property (nonatomic) CMSampleBufferRef sourceSample;
@end

@implementation PixelBuffer
@end

@interface SampleHandler ()<H264EncoderDelegate, FDKAACEncoderDelegate> {
    MongooseDaemon* mongoose;
    //LiveStreamer* streamer;
    TSStreamer* streamer;
    HLSMuxer* hlsStreamer;
    H264Encoder* h264Encoder;
    FDKAACEncoder* aacEncoder;
    InfiniteAudio* infiniteAudio;
    
    dispatch_queue_t queue;
    
    int screenWidth;
    int screenHeight;
    NSTimeInterval lastTimeFrameEncoded;
    
    CMSimpleQueueRef sampleQueue;
    CMSampleBufferRef previousSample;
    CMSampleTimingInfo pInfo[16];
    
    CMTime previousAudioSampleTime;
    CMTime previousVideoSampleTime;
    
    CMTime lastReceivedVideoSampleTime;
    
    int sampleIndex;
    
    bool currentLandscapeMode;
    
    CVPixelBufferPoolRef _rotationPool;
    CVPixelBufferPoolRef _scalingPool;
}

@property (nonatomic) bool requestedLandscapeMode;
@property (nonatomic) CGRect cropRect;
@property (nonatomic) CGRect requestedCropRect;
@property (nonatomic) bool trialMode;
@property (nonatomic) bool trialExpired;
@property (nonatomic) NSDate* trialStarted;
@end

@implementation SampleHandler


static void
show_landscape(struct mg_connection *conn,
            const struct mg_request_info *request_info,
            void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>Switching to landscape mode</body></html>");
    ((__bridge SampleHandler*)user_data).requestedLandscapeMode = true;
}


static void
show_ping(struct mg_connection *conn,
               const struct mg_request_info *request_info,
               void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>ping ok</body></html>");
}

static void
show_portrait(struct mg_connection *conn,
               const struct mg_request_info *request_info,
               void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>Switching to portrait mode</body></html>");
    ((__bridge SampleHandler*)user_data).requestedLandscapeMode = false;
}

static void
show_purchase(struct mg_connection *conn,
              const struct mg_request_info *request_info,
              void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>Purchased ok</body></html>");
    ((__bridge SampleHandler*)user_data).trialMode = false;
}

static void
show_trial(struct mg_connection *conn,
               const struct mg_request_info *request_info,
               void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>Trial mode set</body></html>");
    ((__bridge SampleHandler*)user_data).trialMode = true;
}

static void
show_expired(struct mg_connection *conn,
           const struct mg_request_info *request_info,
           void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", ((__bridge SampleHandler*)user_data).trialExpired ? "expired" : "live");
}

static void
show_rechargeTrial(struct mg_connection *conn,
           const struct mg_request_info *request_info,
           void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>Trial mode set</body></html>");
    ((__bridge SampleHandler*)user_data).trialStarted = [NSDate date];
}


static void
show_crop(struct mg_connection *conn,
          const struct mg_request_info *request_info,
          void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Type: text/html\r\n\r\n");
    mg_printf(conn, "%s", "<html><body>");
    mg_printf(conn, "%s", "<p>Changing crop rect: ");
    
    
    float cropLeft = 0;
    float cropWidth = 1;
    float cropTop = 0;
    float cropHeight = 1;
    char* buff = NULL;
    if((buff = mg_get_var(conn, "crop_left"))) {
        cropLeft = strtof(buff, NULL);
    }
    if((buff = mg_get_var(conn, "crop_width"))) {
        cropWidth = strtof(buff, NULL);
    }
    if((buff = mg_get_var(conn, "crop_top"))) {
        cropTop = strtof(buff, NULL);
    }
    if((buff = mg_get_var(conn, "crop_height"))) {
        cropHeight = strtof(buff, NULL);
    }
    mg_printf(conn, "new rect: %fx%f, %fx%f</body></html>", cropLeft, cropTop, cropWidth, cropHeight);
    ((__bridge SampleHandler*)user_data).requestedCropRect = CGRectMake(cropLeft, cropTop, cropWidth, cropHeight);
}




static void
show_ts(struct mg_connection *conn,
              const struct mg_request_info *request_info,
              void *user_data)
{
    mg_printf(conn, "%s", "HTTP/1.1 200 OK\r\n");
    mg_printf(conn, "%s", "Content-Length:16712244000\r\n");
    //mg_printf(conn, "%s", "Content-Type: video/vnd.dlna.mpeg-tts\r\n\r\n");
    mg_printf(conn, "%s", "Transfer-Encoding: chunked\r\n\r\n");
    
    if(strcmp(request_info->request_method, "GET") || strcmp(request_info->request_method, "get"))
    {
        Buffer* buffer = [[Buffer alloc] initWithSize:1024*1024];
        [((__bridge SampleHandler*)user_data) SubscribeBuffer:buffer];
        
        uint8_t buff[188*10];
        while(((__bridge SampleHandler*)user_data).broadcastActive)
        {
            if([buffer GetWaterLevel] > sizeof(buff)) {
                size_t size = [buffer ReadData:buff maxSize:sizeof(buff)];
                mg_printf(conn, "%x\r\n", size);
                size_t writtenSize = 0;
                size_t packetSize = 0;
                bool connectionInterrupted = false;
                while(writtenSize < size)
                {
                    if((packetSize = mg_write(conn, buff + writtenSize, (int)(size - writtenSize))) <= 0) {
                        connectionInterrupted = true;
                        break;
                    }
                    writtenSize += packetSize;
                }
                if(connectionInterrupted) break;
                mg_printf(conn, "\r\n");
            }
            else usleep(1000);
        }
        [((__bridge SampleHandler*)user_data) UnsubscribeBuffer:buffer];
    }
}

- (void)broadcastStartedWithSetupInfo:(NSDictionary<NSString *,NSObject *> *)setupInfo {
    mongoose = [[MongooseDaemon alloc] init];
    [mongoose startMongooseDaemon:@"8080"];
    
    mg_set_uri_callback(mongoose.ctx, "/landscape", &show_landscape, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/portrait", &show_portrait, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/ts.m2ts", &show_ts, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/ping", &show_ping, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/crop", &show_crop, (__bridge void *)(self));

    mg_set_uri_callback(mongoose.ctx, "/expired", &show_expired, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/recharge", &show_rechargeTrial, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/purchase", &show_purchase, (__bridge void *)(self));
    mg_set_uri_callback(mongoose.ctx, "/trial", &show_trial, (__bridge void *)(self));

    queue = dispatch_queue_create("encoderQueue", NULL);
    //dispatch_set_target_queue(queue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0));
    
    
    CMSimpleQueueCreate(kCFAllocatorDefault, 8, &sampleQueue);
    
    h264Encoder = nil;
    previousSample = nil;
    aacEncoder = nil;
    infiniteAudio = nil;
    _rotationPool = nil;
    _scalingPool = nil;
    
    previousAudioSampleTime.value = 0;
    previousVideoSampleTime.value = 0;
    lastReceivedVideoSampleTime.value = 0;
    sampleIndex = 0;
    previousVideoSampleTime.timescale = 1000000000;
    previousAudioSampleTime.timescale = 1000000000;
    lastReceivedVideoSampleTime.timescale = 1000000000;
    
    currentLandscapeMode = false;
    _requestedLandscapeMode = false;
    _broadcastActive = true;
    
    _cropRect = CGRectMake(0, 0, 1, 1);
    _requestedCropRect = _cropRect;
    
    _trialMode = false;
    _trialStarted = [NSDate date];
    
    streamer = [[TSStreamer alloc] init];
    hlsStreamer = [[HLSMuxer alloc] initWithTargetAddress:@""];
    lastTimeFrameEncoded = [[NSDate date] timeIntervalSince1970];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)),queue, ^{
        [self processQueue];
    });
}

-(bool)trialExpired {
    return _trialMode && ([NSDate date].timeIntervalSince1970 - _trialStarted.timeIntervalSince1970) > TRIAL_DURATION_SECONDS;
}


- (void)broadcastPaused {
    //_broadcastActive = false;
    // User has requested to pause the broadcast. Samples will stop being delivered.
}

- (void)broadcastResumed {
    //_broadcastActive = true;
    // User has requested to resume the broadcast. Samples delivery will resume.
}

-(void) SubscribeBuffer:(Buffer*)buffer {
    if(streamer != nil) {
        [buffer Clear];
        buffer.waitingForRandomAccessPoint = true;
        [streamer SubscribeBuffer:buffer];
    }
}

-(void) UnsubscribeBuffer:(Buffer*)buffer {
    if(streamer != nil) {
        [streamer UnsubscribeBuffer:buffer];
    }
}

- (void)broadcastFinished {
    _broadcastActive = false;
    // User has requested to finish the broadcast.
    [mongoose stopMongooseDaemon];
    if(h264Encoder != nil) {
        [h264Encoder close];
        h264Encoder = nil;
    }
    aacEncoder = nil;
}


-(void)switchLandscapeMode {
    _requestedLandscapeMode = !_requestedLandscapeMode;
}


/*- (CVPixelBufferRef) RotateBuffer:(CVPixelBufferRef)imageBuffer withConstant:(uint8_t)rotationConstant
{
    
    vImage_Error err = kvImageNoError;
    
    
    CVPixelBufferLockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    if(CVPixelBufferGetPixelFormatType(imageBuffer) != kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) return nil;
    if(CVPixelBufferGetPlaneCount(imageBuffer) != 2) return nil;
    
    if(_pool == nil) {
        size_t height = CVPixelBufferGetWidth(imageBuffer);
        size_t width = CVPixelBufferGetHeight(imageBuffer);
        NSMutableDictionary*     attributes;
        attributes = [NSMutableDictionary dictionary];
        
       
        [attributes setObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
        [attributes setObject:[NSNumber numberWithInt:(int)width] forKey: (NSString*)kCVPixelBufferWidthKey];
        [attributes setObject:[NSNumber numberWithInt:(int)height] forKey: (NSString*)kCVPixelBufferHeightKey];
        CVPixelBufferPoolCreate(kCFAllocatorDefault, NULL, (__bridge CFDictionaryRef) attributes, &_pool);
    }
    CVPixelBufferRef rotatedBuffer = NULL;
        if(kCVReturnSuccess != CVPixelBufferPoolCreatePixelBufferWithAuxAttributes(NULL,
                                                            _pool,
                                                            (__bridge CFDictionaryRef)@{
                                                                                        // Opt to fail buffer creation in case of slow buffer consumption
                                                                                        // rather than to exhaust all memory.
                                                                                        (__bridge id)kCVPixelBufferPoolAllocationThresholdKey: @8
                                                                                        }, // aux attributes
                                                            &rotatedBuffer
                                                                                   )) return nil;
   
    if(rotatedBuffer == nil) return nil;
    
    CVPixelBufferLockBaseAddress(rotatedBuffer, 0);
    
    // rotate Y plane
    vImage_Buffer originalYBuffer = { CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0), CVPixelBufferGetHeightOfPlane(imageBuffer, 0),
        CVPixelBufferGetWidthOfPlane(imageBuffer, 0), CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0) };
    vImage_Buffer rotatedYBuffer = { CVPixelBufferGetBaseAddressOfPlane(rotatedBuffer, 0), CVPixelBufferGetHeightOfPlane(rotatedBuffer, 0),
        CVPixelBufferGetWidthOfPlane(rotatedBuffer, 0), CVPixelBufferGetBytesPerRowOfPlane(rotatedBuffer, 0) };
    err = vImageRotate90_Planar8(&originalYBuffer, &rotatedYBuffer, 1, 0.0, kvImageNoFlags);
    
    // rotate UV plane
    vImage_Buffer originalUVBuffer = { CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 1), CVPixelBufferGetHeightOfPlane(imageBuffer, 1),
        CVPixelBufferGetWidthOfPlane(imageBuffer, 1), CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 1) };
    vImage_Buffer rotatedUVBuffer = { CVPixelBufferGetBaseAddressOfPlane(rotatedBuffer, 1), CVPixelBufferGetHeightOfPlane(rotatedBuffer, 1),
        CVPixelBufferGetWidthOfPlane(rotatedBuffer, 1), CVPixelBufferGetBytesPerRowOfPlane(rotatedBuffer, 1) };
    err = vImageRotate90_Planar16U(&originalUVBuffer, &rotatedUVBuffer, 1, 0.0, kvImageNoFlags);
    
    CVPixelBufferUnlockBaseAddress(rotatedBuffer, 0);
    CVPixelBufferUnlockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    return rotatedBuffer;
}*/

-(CGRect)getCropRectPixelsForPlaneWidth:(size_t)planeWidth planeHeight:(size_t)planeHeight {
    CGRect result = CGRectMake(planeWidth * _cropRect.origin.x, planeHeight * _cropRect.origin.y, planeWidth * _cropRect.size.width, planeHeight * _cropRect.size.height);
    result.origin.x = (((int)result.origin.x) / 2) * 2;
    result.origin.y = (((int)result.origin.y) / 2) * 2;
    result.size.width = (((int)result.size.width) / 2) * 2;
    result.size.height = (((int)result.size.height) / 2) * 2;
    return result;
};

- (CVPixelBufferRef) CropBuffer:(CVPixelBufferRef)imageBuffer
{
    
    vImage_Error err = kvImageNoError;
    
    
    CVPixelBufferLockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    if(CVPixelBufferGetPixelFormatType(imageBuffer) != kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) return nil;
    if(CVPixelBufferGetPlaneCount(imageBuffer) != 2) return nil;
    
    if(_scalingPool == nil) {
        size_t height = CVPixelBufferGetHeight(imageBuffer);
        size_t width = CVPixelBufferGetWidth(imageBuffer);
        NSMutableDictionary*     attributes;
        attributes = [NSMutableDictionary dictionary];
        
        
        [attributes setObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
        [attributes setObject:[NSNumber numberWithInt:(int)width] forKey: (NSString*)kCVPixelBufferWidthKey];
        [attributes setObject:[NSNumber numberWithInt:(int)height] forKey: (NSString*)kCVPixelBufferHeightKey];
        CVPixelBufferPoolCreate(kCFAllocatorDefault, NULL, (__bridge CFDictionaryRef) attributes, &_scalingPool);
    }
    CVPixelBufferRef scaledBuffer = NULL;
    if(kCVReturnSuccess != CVPixelBufferPoolCreatePixelBufferWithAuxAttributes(NULL,
                                                                               _scalingPool,
                                                                               (__bridge CFDictionaryRef)@{
                                                                                                           // Opt to fail buffer creation in case of slow buffer consumption
                                                                                                           // rather than to exhaust all memory.
                                                                                                           (__bridge id)kCVPixelBufferPoolAllocationThresholdKey: @8
                                                                                                           }, // aux attributes
                                                                               &scaledBuffer
                                                                               )) return nil;
    
    if(scaledBuffer == nil) return nil;
    
    CVPixelBufferLockBaseAddress(scaledBuffer, 0);
    
    // rotate Y plane
    size_t plane0Width = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
    size_t plane0Height = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
    CGRect pixelsRect = [self getCropRectPixelsForPlaneWidth:plane0Width planeHeight:plane0Height];
    vImage_Buffer originalYBuffer = { CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0) + CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0) * (size_t)pixelsRect.origin.y + (size_t)pixelsRect.origin.x, (size_t)pixelsRect.size.height,
        (size_t)pixelsRect.size.width, CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0) };
    vImage_Buffer rotatedYBuffer = { CVPixelBufferGetBaseAddressOfPlane(scaledBuffer, 0), CVPixelBufferGetHeightOfPlane(scaledBuffer, 0),
        CVPixelBufferGetWidthOfPlane(scaledBuffer, 0), CVPixelBufferGetBytesPerRowOfPlane(scaledBuffer, 0) };
    //err = vImageRotate90_Planar8(&originalYBuffer, &rotatedYBuffer, 1, 0.0, kvImageNoFlags);
    err = vImageScale_Planar8(&originalYBuffer, &rotatedYBuffer, NULL, kvImageNoFlags);
    
    
    // rotate UV plane
    vImage_Buffer originalUVBuffer = { CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 1) + CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 1) * (size_t)(pixelsRect.origin.y/2) + ((((size_t)(pixelsRect.origin.x))>>1)<<1), (size_t)pixelsRect.size.height/2,
        (size_t)pixelsRect.size.width/2, CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 1) };
    vImage_Buffer rotatedUVBuffer = { CVPixelBufferGetBaseAddressOfPlane(scaledBuffer, 1), CVPixelBufferGetHeightOfPlane(scaledBuffer, 1),
        CVPixelBufferGetWidthOfPlane(scaledBuffer, 1), CVPixelBufferGetBytesPerRowOfPlane(scaledBuffer, 1) };
    //err = vImageRotate90_Planar16U(&originalUVBuffer, &rotatedUVBuffer, 1, 0.0, kvImageNoFlags);
    err = vImageScale_CbCr8(&originalUVBuffer, &rotatedUVBuffer, NULL, kvImageNoFlags);
    
    CVPixelBufferUnlockBaseAddress(scaledBuffer, 0);
    CVPixelBufferUnlockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    return scaledBuffer;
}


- (CVPixelBufferRef) RotateBuffer:(CVPixelBufferRef)imageBuffer withConstant:(uint8_t)rotationConstant
{
    
    vImage_Error err = kvImageNoError;
    
    
    CVPixelBufferLockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    if(CVPixelBufferGetPixelFormatType(imageBuffer) != kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) return nil;
    if(CVPixelBufferGetPlaneCount(imageBuffer) != 2) return nil;
    
    if(_rotationPool == nil) {
        size_t height = CVPixelBufferGetWidth(imageBuffer);
        size_t width = CVPixelBufferGetHeight(imageBuffer);
        NSMutableDictionary*     attributes;
        attributes = [NSMutableDictionary dictionary];
        
        
        [attributes setObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
        [attributes setObject:[NSNumber numberWithInt:(int)width] forKey: (NSString*)kCVPixelBufferWidthKey];
        [attributes setObject:[NSNumber numberWithInt:(int)height] forKey: (NSString*)kCVPixelBufferHeightKey];
        CVPixelBufferPoolCreate(kCFAllocatorDefault, NULL, (__bridge CFDictionaryRef) attributes, &_rotationPool);
    }
    CVPixelBufferRef rotatedBuffer = NULL;
    if(kCVReturnSuccess != CVPixelBufferPoolCreatePixelBufferWithAuxAttributes(NULL,
                                                                               _rotationPool,
                                                                               (__bridge CFDictionaryRef)@{
                                                                                                           // Opt to fail buffer creation in case of slow buffer consumption
                                                                                                           // rather than to exhaust all memory.
                                                                                                           (__bridge id)kCVPixelBufferPoolAllocationThresholdKey: @8
                                                                                                           }, // aux attributes
                                                                               &rotatedBuffer
                                                                               )) return nil;
    
    if(rotatedBuffer == nil) return nil;
    
    CVPixelBufferLockBaseAddress(rotatedBuffer, 0);
    
    // rotate Y plane
    vImage_Buffer originalYBuffer = { CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0), CVPixelBufferGetHeightOfPlane(imageBuffer, 0),
        CVPixelBufferGetWidthOfPlane(imageBuffer, 0), CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0) };
    vImage_Buffer rotatedYBuffer = { CVPixelBufferGetBaseAddressOfPlane(rotatedBuffer, 0), CVPixelBufferGetHeightOfPlane(rotatedBuffer, 0),
        CVPixelBufferGetWidthOfPlane(rotatedBuffer, 0), CVPixelBufferGetBytesPerRowOfPlane(rotatedBuffer, 0) };
    err = vImageRotate90_Planar8(&originalYBuffer, &rotatedYBuffer, 1, 0.0, kvImageNoFlags);
    
    // rotate UV plane
    vImage_Buffer originalUVBuffer = { CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 1), CVPixelBufferGetHeightOfPlane(imageBuffer, 1),
        CVPixelBufferGetWidthOfPlane(imageBuffer, 1), CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 1) };
    vImage_Buffer rotatedUVBuffer = { CVPixelBufferGetBaseAddressOfPlane(rotatedBuffer, 1), CVPixelBufferGetHeightOfPlane(rotatedBuffer, 1),
        CVPixelBufferGetWidthOfPlane(rotatedBuffer, 1), CVPixelBufferGetBytesPerRowOfPlane(rotatedBuffer, 1) };
    err = vImageRotate90_Planar16U(&originalUVBuffer, &rotatedUVBuffer, 1, 0.0, kvImageNoFlags);
    
    CVPixelBufferUnlockBaseAddress(rotatedBuffer, 0);
    CVPixelBufferUnlockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    return rotatedBuffer;
}

-(bool)croppingRequested {
    return (_cropRect.size.width + _cropRect.size.height) < 1.999;
}

-(void)processQueue {
    
    @try
    {

        double difference = 0.1;
        if(currentLandscapeMode) {
            difference = 0.2;
        }

        if(CMSimpleQueueGetCount(sampleQueue) == 0 && previousSample != nil)
        {
            if(fabs([[NSDate date] timeIntervalSince1970] - lastTimeFrameEncoded) > difference && previousSample != nil) {
                CMTime timeStamp = CMSampleBufferGetPresentationTimeStamp(previousSample);
                timeStamp.value += ([[NSDate date] timeIntervalSince1970] - lastTimeFrameEncoded)*timeStamp.timescale;
                CMItemCount count;
                if(noErr == CMSampleBufferGetSampleTimingInfoArray(previousSample, 16, pInfo, &count))
                {
                    for (CMItemCount i = 0; i < count; i++)
                    {
                        pInfo[i].decodeTimeStamp = kCMTimeInvalid;
                        pInfo[i].presentationTimeStamp = timeStamp;
                        
                    }
                    CMSampleBufferRef sout;
                    if(noErr == CMSampleBufferCreateCopyWithNewTiming(kCFAllocatorDefault, previousSample, count, pInfo, &sout))
                    {
                        if(noErr != CMSimpleQueueEnqueue(sampleQueue, sout))
                        {
                            CFRelease(sout);
                        }
                    }
                }
            }
        }
        
        CMSampleBufferRef sampleBuffer;
        while((sampleBuffer = (CMSampleBufferRef)CMSimpleQueueDequeue(sampleQueue)) != nil)
        {
            
            CMTime timeStamp;
            timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            
            if(previousSample != nil) {
                CMTime previousTimeStamp = CMSampleBufferGetPresentationTimeStamp(previousSample);
                if(previousTimeStamp.value > timeStamp.value) {
                    CFRelease(sampleBuffer);
                    continue;
                }
            }
            
            CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
            if(imageBuffer == nil) continue;
            size_t width = CVPixelBufferGetWidth(imageBuffer);
            size_t height = CVPixelBufferGetHeight(imageBuffer);
            
            if(currentLandscapeMode) {
                screenWidth = (int)height;//(int)1280;//(int)width;
                screenHeight = (int)width;//(int)720;//(int)height;
            }
            else {
                screenWidth = (int)width;
                screenHeight = (int)height;
            }
            
            
            if(h264Encoder == nil) {
                
                int encoderWidth = screenWidth * ENCODER_HEIGHT / screenHeight;// 1280;
                int encoderHeight = ENCODER_HEIGHT;//(screenHeight * 1280/ screenWidth);
                if(fabs(16.0/9.0 - (double)screenHeight / (double)screenWidth) < 0.1)
                {
                    encoderWidth = ENCODER_WIDTH;
                    encoderHeight = ENCODER_HEIGHT;
                } else if(fabs(4.0/3.0 - (double)screenHeight / (double)screenWidth) < 0.1)
                {
                    encoderWidth = ENCODER_WIDE_WIDTH;
                    encoderHeight = ENCODER_HEIGHT;
                }
                
               
                encoderWidth = (encoderWidth * _cropRect.size.width);
                encoderHeight = (encoderHeight * _cropRect.size.height);

                encoderWidth = (encoderWidth / 2) * 2;
                encoderHeight = (encoderHeight / 2) * 2;
                
                h264Encoder = [[H264Encoder alloc] initWithWidth:encoderWidth height:encoderHeight bitrate:ENCODER_BITRATE*1000 framerate:30];
                h264Encoder.delegate = self;
            }
            
            CVImageBufferRef rotatedBuffer = imageBuffer;
            if(currentLandscapeMode) rotatedBuffer = [self RotateBuffer:imageBuffer withConstant:0];
            if(rotatedBuffer != nil)
            {
                CVImageBufferRef croppedBuffer = rotatedBuffer;
                if([self croppingRequested]) {
                    croppedBuffer = [self CropBuffer:rotatedBuffer];
                }
                [h264Encoder putCVPixelBuffer:croppedBuffer withTimestamp:timeStamp];
                if(croppedBuffer != rotatedBuffer) {
                    CFRelease(croppedBuffer);
                }
                if(rotatedBuffer != imageBuffer) {
                    CFRelease(rotatedBuffer); //release only rotated buffer
                }
            }
            CMTime audioTimestamp = timeStamp;
            audioTimestamp.value -= timeStamp.timescale;
            if(infiniteAudio != nil) [infiniteAudio consumeToTime:audioTimestamp];
            previousVideoSampleTime = timeStamp;
            if(previousSample != nil) CFRelease(previousSample);
            previousSample = sampleBuffer;
            lastTimeFrameEncoded = [[NSDate date] timeIntervalSince1970];
            
        }
        
    }
    @catch(NSException *exception)
    {
        
    }
    
    if(currentLandscapeMode != _requestedLandscapeMode ||
       !CGRectEqualToRect(_cropRect, _requestedCropRect))  {
        if(h264Encoder != nil) {
            [h264Encoder close];
            h264Encoder = nil;
            if(streamer) [streamer flush];
        }
        currentLandscapeMode = _requestedLandscapeMode;
        _cropRect = _requestedCropRect;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)),queue, ^{
        [self processQueue];
    });
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer withType:(RPSampleBufferType)sampleBufferType {
    //NSLog(@"sample received");
    if(self.trialMode && self.trialExpired) return;
    
    switch (sampleBufferType) {
        case RPSampleBufferTypeVideo:
            // Handle video sample buffer
        {
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferAttachmentKey_DroppedFrameReason, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferAttachmentKey_DroppedFrameReasonInfo, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferDroppedFrameReason_Discontinuity, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferDroppedFrameReason_OutOfBuffers, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferDroppedFrameReason_FrameWasLate, NULL)) break;;
            if(!CMSampleBufferDataIsReady(sampleBuffer) || !CMSampleBufferIsValid(sampleBuffer)) break;
            
            
            
            
            CMTime currentTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            if(currentLandscapeMode || [self croppingRequested]) {
                if(CMTimeGetSeconds(currentTime) - CMTimeGetSeconds(lastReceivedVideoSampleTime) < 0.1) break;
            }
            else {
                if(CMTimeGetSeconds(currentTime) - CMTimeGetSeconds(lastReceivedVideoSampleTime) < 0.05) break;
            }
            
            lastReceivedVideoSampleTime = currentTime;
            
            
            CFRetain(sampleBuffer);
            
            @try
            {
                if(previousSample != nil)
                {
                    CMTime timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
                    CMTime previousTimeStamp = CMSampleBufferGetPresentationTimeStamp(previousSample);
                    if(previousTimeStamp.value > timeStamp.value) {
                        CFRelease(sampleBuffer);
                        sampleBuffer = nil;
                    }
                }
                
                if(sampleBuffer != nil && noErr != CMSimpleQueueEnqueue(sampleQueue, sampleBuffer))
                {
                    CFRelease(sampleBuffer);
                    sampleBuffer = nil;
                }
                else sampleBuffer = nil;
            }
            @catch(NSException *exception)
            {
                
            }
            @finally
            {
                if(sampleBuffer != nil) {
                    CFRelease(sampleBuffer);
                    sampleBuffer = nil;
                }
            }
        }
            break;
        case RPSampleBufferTypeAudioApp:
        {
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferAttachmentKey_DroppedFrameReason, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferAttachmentKey_DroppedFrameReasonInfo, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferDroppedFrameReason_Discontinuity, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferDroppedFrameReason_OutOfBuffers, NULL)) break;;
            if(CMGetAttachment(sampleBuffer, kCMSampleBufferDroppedFrameReason_FrameWasLate, NULL)) break;;
            if(!CMSampleBufferDataIsReady(sampleBuffer) || !CMSampleBufferIsValid(sampleBuffer)) break;

            CMTime timeStamp;
            timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            if(timeStamp.value < previousAudioSampleTime.value || CMTimeGetSeconds(timeStamp) < CMTimeGetSeconds(previousVideoSampleTime) - 1) break;

            __block CMSampleBufferRef sampleCopy = sampleBuffer;
            CFRetain(sampleCopy);

            dispatch_async(queue, ^{
                @try
                {
                    if(self->aacEncoder == nil) {
                        CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleCopy);
                        const AudioStreamBasicDescription* const asbd = CMAudioFormatDescriptionGetStreamBasicDescription(formatDescription);
                        self->aacEncoder = [[FDKAACEncoder alloc] initWithSampleRate:asbd->mSampleRate channels:asbd->mChannelsPerFrame bitrate:64000];
                        self->aacEncoder.delegate = self;
                        self->infiniteAudio = [[InfiniteAudio alloc] initWithSampleRate:asbd->mSampleRate channels:asbd->mChannelsPerFrame targetEncoder:self->aacEncoder];
                    }
                    [self->infiniteAudio putCMSampleBuffer:sampleCopy];
                }
                @catch(NSException *exception)
                {
                    
                }
                @finally
                {
                    if(sampleCopy != nil) {
                        CFRelease(sampleCopy);
                        sampleCopy = nil;
                    }
                }
            });
        }
            // Handle audio sample buffer for app audio
            break;
        case RPSampleBufferTypeAudioMic:
            // Handle audio sample buffer for mic audio
            break;
            
        default:
            break;
    }
    //CMSampleBufferInvalidate(sampleBuffer);
}

- (void)compressedVideoDataReceived:(CMSampleBufferRef)sampleBuffer {
    @try
    {
        CFRetain(sampleBuffer);
        dispatch_async(queue, ^{
            [self->streamer putVideoSample:sampleBuffer];
            [self->hlsStreamer putVideoSample:sampleBuffer];
            CFRelease(sampleBuffer);
        });
    }
    @catch(NSException* e)
    {
    }
}

-(void) compressedAudioDataReceived:(uint8_t*)data size:(size_t)size pts:(CMTime)pts {
    //dispatch_async(queue, ^{
    [streamer putAudioSample:data size:size pts:pts];
    [hlsStreamer putAudioSample:data size:size pts:pts];
    //});
}


@end
